TEMPLATE = app
TARGET = tvout-app
tvout-app.files = tvout-app
tvout-app.path = $${PREFIX}/bin
DEPENDPATH += .
INCLUDEPATH += .
INSTALLS += tvout-app
CONFIG += link_pkgconfig
PKGCONFIG += tvout-ctl

# Input
HEADERS += tvout-app.h
SOURCES += tvout-app.cpp
