/*
 * Nokia N9/N950 TV out control
 * Copyright (C) 2011-2012  Ville Syrjälä <syrjala@sci.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include <QApplication>
#include <QPushButton>
#include <QLabel>
#include <QHBoxLayout>
#include <QSocketNotifier>
#include <QString>
#include <QVBoxLayout>
#include <QWidget>
#include <QMainWindow>

#include "tvout-app.h"

extern "C" void tvout_notify(void *data, enum TVoutCtlAttr attr, int value)
{
	TVoutApp *app = static_cast<TVoutApp*>(data);

	return app->do_tvout_notify(attr, value);
}

void TVoutApp::do_tvout_notify(enum TVoutCtlAttr attr, int value)
{
	switch (attr) {
	case TVOUT_CTL_ENABLE:
		enable_changed(value);
		break;
	case TVOUT_CTL_TV_STD:
		tv_std_changed(value);
		break;
	case TVOUT_CTL_ASPECT:
		aspect_changed(value);
		break;
	case TVOUT_CTL_SCALE:
		scale_changed(value);
		break;
	default:
		break;
	}
}

void TVoutApp::enable_changed(int value)
{
	static const char *labels[] = {
		"OFF",
		"ON",
	};

	if (value < 0 || value > 1)
		return;

	enable_button->setText(labels[value]);
}

void TVoutApp::tv_std_changed(int value)
{
	static const char *labels[] = {
		"PAL",
		"NTSC",
	};

	if (value < 0 || value > 1)
		return;

	tv_std_button->setText(labels[value]);
}

void TVoutApp::aspect_changed(int value)
{
	static const char *labels[] = {
		"4:3",
		"16:9",
	};

	if (value < 0 || value > 1)
		return;

	aspect_button->setText(labels[value]);
}

void TVoutApp::scale_changed(int value)
{
	if (value < 0 || value > 100)
		return;

	scale_label->setText(QString("%1").arg(value));

	scale_dec_button->setEnabled(value > 0);
	scale_inc_button->setEnabled(value < 100);
}

void TVoutApp::enable_button_clicked()
{
	tvout_ctl_set(tvout_ctl,
		      TVOUT_CTL_ENABLE,
		      !tvout_ctl_get(tvout_ctl, TVOUT_CTL_ENABLE));
}

void TVoutApp::tv_std_button_clicked()
{
	tvout_ctl_set(tvout_ctl,
		      TVOUT_CTL_TV_STD,
		      !tvout_ctl_get(tvout_ctl, TVOUT_CTL_TV_STD));
}

void TVoutApp::aspect_button_clicked()
{
	tvout_ctl_set(tvout_ctl,
		      TVOUT_CTL_ASPECT,
		      !tvout_ctl_get(tvout_ctl, TVOUT_CTL_ASPECT));
}

void TVoutApp::scale_dec_button_clicked()
{
	int value;

	value = tvout_ctl_get(tvout_ctl, TVOUT_CTL_SCALE) - 1;
	if (value < 0 || value > 100)
		return;

	tvout_ctl_set(tvout_ctl,
		      TVOUT_CTL_SCALE,
		      value);
}

void TVoutApp::scale_inc_button_clicked()
{
	int value;

	value = tvout_ctl_get(tvout_ctl, TVOUT_CTL_SCALE) + 1;
	if (value < 0 || value > 100)
		return;

	tvout_ctl_set(tvout_ctl,
		      TVOUT_CTL_SCALE,
		      value);
}

QLayout *TVoutApp::create_ui_enable()
{
	QHBoxLayout *hbox = new QHBoxLayout;

	QWidget *label = new QLabel("TV out");
	hbox->addWidget(label);

	enable_button = new QPushButton;
	enable_button->setText("OFF");
	enable_button->setText("ON");
	hbox->addWidget(enable_button);

	enable_changed(tvout_ctl_get(tvout_ctl, TVOUT_CTL_ENABLE));

	QObject::connect(enable_button, SIGNAL(clicked()), this, SLOT(enable_button_clicked()));

	return hbox;
}

QLayout *TVoutApp::create_ui_tv_std()
{
	QHBoxLayout *hbox = new QHBoxLayout;

	QWidget *label = new QLabel("Video format");
	hbox->addWidget(label);

	tv_std_button = new QPushButton;
	tv_std_button->setText("PAL");
	tv_std_button->setText("NTSC");
	hbox->addWidget(tv_std_button);

	tv_std_changed(tvout_ctl_get(tvout_ctl, TVOUT_CTL_TV_STD));

	QObject::connect(tv_std_button, SIGNAL(clicked()), this, SLOT(tv_std_button_clicked()));

	return hbox;
}

void TVoutApp::tvout_ctl_fd_activated()
{
	notifier->setEnabled(false);
	tvout_ctl_fd_ready(tvout_ctl);
	notifier->setEnabled(true);
}

QLayout *TVoutApp::create_ui_aspect()
{
	QHBoxLayout *hbox = new QHBoxLayout;

	QWidget *label = new QLabel("Aspect ratio");
	hbox->addWidget(label);

	aspect_button = new QPushButton;
	aspect_button->setText("4:3");
	aspect_button->setText("16:9");
	hbox->addWidget(aspect_button);

	aspect_changed(tvout_ctl_get(tvout_ctl, TVOUT_CTL_ASPECT));

	QObject::connect(aspect_button, SIGNAL(clicked()), this, SLOT(aspect_button_clicked()));

	return hbox;
}

QLayout *TVoutApp::create_ui_scale()
{
	QHBoxLayout *hbox = new QHBoxLayout;

	QWidget *label = new QLabel("Scale");
	hbox->addWidget(label);

	scale_dec_button = new QPushButton("<");
	hbox->addWidget(scale_dec_button);

	scale_label = new QLabel("0");
	hbox->addWidget(scale_label);

	scale_inc_button = new QPushButton(">");
	hbox->addWidget(scale_inc_button);

	scale_changed(tvout_ctl_get(tvout_ctl, TVOUT_CTL_SCALE));

	QObject::connect(scale_dec_button, SIGNAL(clicked()), this, SLOT(scale_dec_button_clicked()));
	QObject::connect(scale_inc_button, SIGNAL(clicked()), this, SLOT(scale_inc_button_clicked()));

	return hbox;
}

QLayout *TVoutApp::create_ui()
{
	QVBoxLayout *vbox = new QVBoxLayout;

	vbox->addLayout(create_ui_enable());
	vbox->addLayout(create_ui_tv_std());
	vbox->addLayout(create_ui_aspect());
	vbox->addLayout(create_ui_scale());

	return vbox;
}

TVoutApp::TVoutApp(QWidget *parent)
	: QWidget(parent),
	  notifier(0)
{
	setWindowTitle("TV-out control");

	tvout_ctl = tvout_ctl_init(tvout_notify, this);

	if (tvout_ctl) {
		notifier = new QSocketNotifier(tvout_ctl_fd(tvout_ctl), QSocketNotifier::Read);
		QObject::connect(notifier, SIGNAL(activated(int)), this, SLOT(tvout_ctl_fd_activated()));
	}

	setLayout(create_ui());

	if (!tvout_ctl)
		setEnabled(false);
}

TVoutApp::~TVoutApp()
{
	delete notifier;
	tvout_ctl_exit(tvout_ctl);
}

int main(int argc, char *argv[])
{
	QApplication app(argc, argv);

	TVoutApp window;

	window.show();

	return app.exec();
}
