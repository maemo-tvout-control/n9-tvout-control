/*
 * Nokia N9/N950 TV out control
 * Copyright (C) 2011-2012  Ville Syrjälä <syrjala@sci.fi>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef TVOUTAPP_H
#define TVOUTAPP_H

#include <QWidget>
#include <tvout-ctl.h>

class QSocketNotifier;
class QPushButton;
class QLabel;
class QLayout;

class TVoutApp
	: public QWidget
{
	Q_OBJECT

public:
	TVoutApp(QWidget * parent = 0);
	virtual ~TVoutApp();

	void do_tvout_notify(TVoutCtlAttr attr, int value);

private:
	QPushButton *enable_button;
	QPushButton *tv_std_button;
	QPushButton *aspect_button;
	QPushButton *scale_dec_button;
	QPushButton *scale_inc_button;
	QLabel *scale_label;

	QSocketNotifier *notifier;

	TVoutCtl *tvout_ctl;

	QLayout *create_ui();
	QLayout *create_ui_enable();
	QLayout *create_ui_tv_std();
	QLayout *create_ui_aspect();
	QLayout *create_ui_scale();

	void enable_changed(int value);
	void tv_std_changed(int value);
	void aspect_changed(int value);
	void scale_changed(int value);

private slots:
	void enable_button_clicked();
	void tv_std_button_clicked();
	void aspect_button_clicked();
	void scale_dec_button_clicked();
	void scale_inc_button_clicked();
	void tvout_ctl_fd_activated();
};

#endif
